class Event {
    static isLocked(id) {
        return window.localStorage.getItem(id);
    }

    static lock(id) {
        window.localStorage.setItem(id, true);
    }
}

export default Event;
