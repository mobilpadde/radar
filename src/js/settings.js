export default {
    skillPerMinionPerSecond: 0.01 * Math.random(),
    skills: [
        'Move Speed',           // Done
        'Attack Speed',         // Done
        'Health Regeneration',
        'Health',               // Done
        'Luck',                 // Done
        'Damage',               // Done
        'Attack Range',         // Done
        'Respawn Time',         // Done
        'Defence',              // Done
    ].sort(() => Math.random() - 0.5),

    initialSkillPoints: 0.15,
    initialMinions: 3,
    initialAreas: 3,

    initialRespawnTime: 2,
    initialAttackSpeed: 1.05,
    initialDefenceMultiplier: 0,
    initialDamageMultiplier: 0,

    initialHealth: [30, 80],
    initialSize: [3, 6],

    skillCost: 0.3 * Math.random(),
    skillCostIncrease: 0.05,

    arenaSize: 300,
};