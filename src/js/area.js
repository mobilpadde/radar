import Victor from 'victor';

import util from './util';
import settings from './settings';
import Enemy from './enemy';

export default class Area{
    constructor(sz, center, map, player = false, border = 4) {
        this.hue = ~~(Math.random() * 360);
        this.border = border;

        this.size = sz;
        this.halfSize = sz / 2;

        this.center = center;
        this.positions = [];
        this.mouse = false;
        this.clicked = false;

        this.skillPoints = 0;

        this.rot = Math.PI / 2 * 3;

        this.skills = {};
        settings.skills
            .forEach((s) => this.skills[s] = 0.01);

        (',' + settings.skills.join())
            .repeat(5)
            .split(',')
            .sort(() => Math.random() - 0.5)
            .slice(0, 10)
            .filter((s) => s !== '')
            .forEach((s) => this.skills[s] += settings.initialSkillPoints);

        this.map = map;

        this.player = player;
        this.minions = [];
    }

    initialize(areas) {
        this.generate();
        
        new Array(settings.initialMinions).fill(0).forEach(() => this.respawn(areas));
        setTimeout(() => this.respawn(areas), (settings.initialRespawnTime - this.skills['Respawn Time']) * 10000);
    }

    respawn(areas) {
        const { center, map, hue, positions, skills } = this;

        const m = new Enemy(center.clone(), map, hue, this.skills['Luck']);
        m.setPosition(positions, center);
        m.setSkills(skills);
        m.attack(areas);
        this.minions.push(m);
    }

    getSkillPoint() {
        this.skillPoints += this.minions.length * settings.skillPerMinionPerSecond;
    }

    skillUp(s) {
        if (!s) {
            return false;
        }

        const cost = ((settings.skillCost * 10) * (settings.skillCostIncrease * 10)) / 10 + this.skills[s];
        if (this.skillPoints < cost) {
            return false;
        }

        this.skillPoints -= cost;
        this.skills[s] += settings.skillCost;
        if (this.skills[s] >= 1) {
            this.skills[s] -= this.skills[s] % 1;

            this.regenerate();
            return false;
        }

        this.regenerate();
        return true;
    }

    rndSkillUp() {
        const s = Object.keys(this.skills)[~~(Math.random() * settings.skills.length)];
        return this.skillUp(s);
    }

    generate() {
        this.regenerate();
        this.minions.map((m) => m.setPosition(this.positions, this.center));
    }

    regenerate() {
        const halfSkills = settings.skills.length / 2;
        const step = Math.PI / halfSkills;

        const { center, size } = this;

        this.positions = [];
        Object.values(this.skills)
            .forEach((s) => {
                const pos = new Victor(
                    center.x + Math.cos(this.rot) * (size * s),
                    center.y + Math.sin(this.rot) * (size * s),
                );

                const full = new Victor(
                    center.x + Math.cos(this.rot) * size,
                    center.y + Math.sin(this.rot) * size,
                );

                this.positions.push({ pos, full });
                this.rot += step;
            });

        this.positions.push(this.positions[0], this.positions[this.positions.length - 1]);
        this.minions.map((m) => m.setSkills(this.skills));
    }

    draw(ctx) {
        const { positions, hue, mouse, center, clicked, skills } = this;

        const fillColor = `hsla(${hue}, 25%, 70%, 0.5)`;
        const borderColor = `hsla(${hue}, 25%, 70%, 0.3)`;

        for (let i = 0; i < positions.length - 1; i++) {
            const p0 = positions[i].pos;
            const p1 = positions[i + 1].pos;

            util.drawSegment(this, ctx, p0, p1, fillColor, borderColor);
        };

        this.drawText(ctx);

        if (mouse) {
            for (let i = 0; i < positions.length - 1; i++) {
                const p0 = positions[i];
                const p1 = positions[i + 1];
    
                if (util.inTriangle(mouse, p0.full, p1.full, center)) {
                    const fillColorNext = `hsla(${hue}, 50%, 70%, 0.5)`;
                    util.drawSegment(this, ctx, p0.pos, p1.pos, fillColorNext, 'transparent');

                    if (clicked) {
                        this.skillUp(Object.keys(skills)[i]);
                    }

                    this.clicked = false;
                }
            };
        }

        return this;
    }

    drawText(ctx) {
        const { positions, hue, mouse, center, skills, player } = this;
        if (!player) {
            return;
        }

        const textColor = `hsla(${hue}, 25%, 70%, 0.3)`;
        const textColorHovered = `hsla(${hue}, 25%, 70%, 1)`;

        for (let i = 0; i < Object.keys(skills).length; i++) {
            const p0 = positions[i].full;
            const p1 = positions[i + 1].full;

            const dX = p0.distanceX(center);
            let alignment = 'center';
            if (dX > 5) {
                alignment = 'left';
            } else if (dX < -5) {
                alignment = 'right';
            }
            ctx.textAlign = alignment;

            let pad = 0;
            let baseline = 'middle';
            if (p0.y < center.y) {
                baseline = 'bottom';
            } else if (p0.y > center.y) {
                baseline = 'top';
                pad = 12;
            }
            ctx.textBaseline = baseline;

            const skill = Object.keys(this.skills)[i];
            let cost = '';

            const inTri = util.inTriangle(mouse, p0, p1, center);
            if (inTri) {
                ctx.fillStyle = textColorHovered;
                cost += ` (${((((settings.skillCost * 10) * (settings.skillCostIncrease * 10)) / 10 + this.skills[skill]) * 10).toFixed(2)})`;
            } else {
                ctx.fillStyle = textColor;
            }

            ctx.textAlign = 'left';
            ctx.font = 'bold 24px sans-serif';
            let { width } = ctx.measureText(skill);
            let costWidth = 0;
            if (dX > 5) {
                costWidth = width;
                width = 0;
            }

            ctx.fillText(skill, p0.x - width, p0.y);
            ctx.font = 'bold 12px sans-serif';
            ctx.fillText(cost, p0.x + costWidth, p0.y + pad);


        }
    }

    drawMinions(ctx) {
        const { positions, center } = this;
        this.minions.map((m) => m.wander(positions, center, this.skills['Move Speed']).draw(ctx));
    }

    move(x, y) {
        if (this.player) {
            this.mouse = new Victor(x, y);
        }
    }

    click() {
        this.clicked = true;
    }
}