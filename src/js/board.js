class Board{
    constructor(w, h) {
        let b = document.createElement('canvas');

        b.id = 'board';
        b.width = w;
        b.height = h;

        this.ctx = b.getContext('2d');
        this.board = b;

        document.body.appendChild(b);
    }

    destroy() {
        document.body.removeChild(this.board);

        this.board = null;
        this.ctx = null;
    }

    listen(e, fn) {
        this.board.addEventListener(e, fn);
    }
}

export default Board;
