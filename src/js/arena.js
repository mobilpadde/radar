import Victor from 'victor';

import settings from './settings';

export default class Arena{
    constructor(center, sz = 350, brdr = 4, col = `rgba(150, 150, 150, 0.05)`) {
        this.border = brdr;
        this.color = col;

        this.size = sz;
        this.halfSize = sz / 2;

        this.center = center;
        this.rot = Math.PI / 2 * 3;

        this.positions = [];
    }

    generate() {
        const halfSkills = settings.skills.length / 2;
        const step = Math.PI / halfSkills;

        const { center, size } = this;

        let last = new Victor();
        new Array(settings.skills.length)
            .fill(0)
            .forEach(() => {
                last = new Victor(
                    center.x + Math.cos(this.rot) * size,
                    center.y + Math.sin(this.rot) * size,
                );

                this.rot += step;
                const pos = new Victor(
                    center.x + Math.cos(this.rot) * size,
                    center.y + Math.sin(this.rot) * size,
                );

                this.positions.push(last, pos);
                this.rot += step;
                last = pos;
            });
    }

    draw(ctx) {
        const { positions, center, color, border } = this;

        for (let i = 0; i < positions.length - 1; i++) {
            const p0 = positions[i];
            const p1 = positions[i + 1];

            ctx.beginPath();
            ctx.moveTo(p0.x, p0.y);
            ctx.lineTo(p1.x, p1.y);
            ctx.lineTo(center.x, center.y);
            ctx.closePath();

            ctx.strokeStyle = color;
            ctx.lineWidth = border;
            ctx.stroke();
        };
    }
}