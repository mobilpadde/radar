import Victor from 'victor';

import settings from './settings';
import Board from './board';
import Arena from './arena';
import Area from './area';

import '../styles/app.styl';

(() => {
    let w = window.innerWidth;
    let h = window.innerHeight;

    let topLeft = new Victor(0, 0);
    let bottomRight = new Victor(w, h);
    let center = new Victor(topLeft.x + settings.arenaSize * 1.8, bottomRight.y - settings.arenaSize * 1.25);

    let map = {
        w, h,
        topLeft, bottomRight,
        center
    };

    const board = new Board(w, h);

    const arena = new Arena(center, settings.arenaSize, 2);
    arena.generate();

    const areas = new Array(settings.initialAreas)
        .fill(0)
        .map((_, i) => new Area(settings.arenaSize, center, map, i === settings.initialAreas - 1));

    areas.map((a, i) => {
        a.initialize(
            areas
                .filter((_, j) => i !== j)
                .reduce((acc, _a) => {
                    acc.push(_a);
                    return acc;
                }, [])
        );
    });
    const enemyAreas = areas.filter((s) => !s.player);

    setInterval(() => areas.map((a) => a.getSkillPoint()), 1000);
    setInterval(() => enemyAreas.map((e) => e.rndSkillUp()), 4000);

    board.listen('mousemove', ({ clientX, clientY }) => areas.map((a) => a.move(clientX, clientY)));
    board.listen('mouseup', () => areas.map((a) => a.click()));

    const draw = () => {
        let ctx = board.ctx;

        ctx.fillStyle = `rgba(224, 238, 255, 1)`;
        ctx.fillRect(0, 0, w, h);

        let size = 48;
        ctx.font = `bold ${size}px sans-serif`;
        ctx.textBaseline = 'top';
        ctx.textAlign = 'left';
        ctx.fillStyle = 'rgb(50, 50, 50)';

        let texts = [
            'Conquer',
            'The',
            'Arena',
        ];

        let i = 1;
        for (let text of texts) {
            ctx.fillText(text, size, size * i);
            i += 1.25;
        }

        size = 18;
        ctx.font = `${size}px sans-serif`;
        ctx.textAlign = 'right';
        const a = areas[areas.length - 1];
        const s = a.skills;
        texts = [
            `Your minions are currently`,
            `moving at a speed of ${(s['Move Speed'] * 100).toFixed(0)}%`,
            `and can attack every ${((settings.initialAttackSpeed - s['Attack Speed']) * 10).toFixed(2)}/s.`,
            '',

            `They also have a health`,
            `regeneration of ${s['Health Regeneration'].toFixed(2)}/s,`,
            `which makes their health multiplier ${s['Health'].toFixed(2)}.`,
            '',

            `At the moment they have`,
            `an attack range of ${(s['Attack Range'] * 100).toFixed(0)}% their size.`,
            `Your luck for respawning a`,
            `giant minion is ${(s['Luck'] * 100).toFixed(0)}%.`,
            '',

            `At the moment the minions are`,
            `attacking with a damage of ${(s['Damage'] * 100).toFixed(0)}%,`,
            `and their defence is ${(s['Defence'] * 100).toFixed(0)}%.`,
            '',

            `Their current respawn time is ${((settings.initialRespawnTime - s['Respawn Time']) * 10).toFixed(2)}s.`,
            '',

            `Also, you currently have ${(a.skillPoints * 10).toFixed(2)}`,
            `remaining skill points for upgrades.`,
            '',

            `Come on, Conquer The Arena!`,
            '',
        ].reverse();

        i = 1;
        for (let text of texts) {
            ctx.fillText(text, map.bottomRight.x - 48, map.bottomRight.y - size * i);
            i += 1.35;
        }
        
        arena.draw(ctx);
        areas.map((a) => a.draw(ctx));
        areas.map((a) => a.drawMinions(ctx));
        requestAnimationFrame(draw);
    };


    requestAnimationFrame(draw);
})();
