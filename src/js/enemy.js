import Creature from './creature';
import settings from './settings';

class Enemy extends Creature{
    constructor(v, map, color, luck) {
        super(v, map, color);

        const giant = Math.random() <= luck;
        this.maxHp = giant ? settings.initialHealth[1] : settings.initialHealth[0];
        this.hp = this.maxHp;

        this.size = giant ? settings.initialSize[1] : settings.initialSize[0];
        this.speed = 0.05 / this.size;
        this.def = 0.5 - 0.5 / this.size;
        this.defMult = settings.initialDefenceMultiplier;
        this.dmg = 0.75 - 0.75 / this.size;
        this.dmgMult = settings.initialDamageMultiplier;
    }

    attack(areas) {
        if (this.dead) {
            return;
        }

        for (let a of areas) {
            let ret = false;
            for (let e of a.minions) {
                if (this._attack(e)) {
                    ret = true;
                    break;
                }
            }

            if (ret) {
                break;
            }
        }

        setTimeout(() => this.attack(areas), this.attackSpeed * 10000);
    }

    _attack(whom) {
        const { pos, radius, dmg, dmgMult } = this;

        if (pos.distance(whom.pos) < radius) {
            return whom.takeDmg(dmg * dmgMult, whom.pos);
        }

        return false;
    }
}

export default Enemy;
