import Victor from 'victor';

import util from './util';
import settings from './settings';

class Creature{
    constructor(v, map, hue) {
        this.hue = hue;
        this.map = map;
        this.dream = map.center;
        this.pos = v || new Victor(0, 0);

        this.maxHp = settings.initialHealth[0];
        this.hp = this.maxHp;
        
        this.speed = 0;
        this.def = 0;
        this.defMult = 0;

        this.attackSpeed = settings.initialAttackSpeed;

        this.size = settings.initialSize[0];
        this.radius = this.size * 4;

        this.dead = false;
    }

    setPosition(p, center) {
        this.pos = p[~~(Math.random() * p.length)].pos.clone();
        this.pos.mix(center);
    }

    setSkills(s) {
        this.attackSpeed = settings.initialAttackSpeed - s['Attack Speed'];
        this.dmgMult = settings.initialDamageMultiplier + s['Damage'];
        this.radius = this.size * 4 * s['Attack Range'];
    }

    die() {
        this.hp = 0;
        this.dead = true;
    }

    takeDmg(dmg, v) {
        if (this.hp <= 0) {
            return false;
        }

        this.hp -= (dmg * (1 - this.def * this.defMult)) * 50;
        this.bounce(v);
        if (this.hp <= 0) {
            this.die();
        }

        return true;
    }

    bounce(v) {
        const { pos, speed } = this;
        const n = new Victor(5, 5);
        n.multiply(new Victor(
            pos.x < v.x ? -speed : speed,
            pos.y < v.y ? -speed : speed
        ));
        n.multiplyScalar(7.5);

        pos.add(n);
    }

    wander(positions, center, speed) {
        if (this.dead) {
            return this;
        }

        if (this.pos.distanceSq(this.dream) < 100) {
            this.dream = positions[~~(Math.random() * positions.length)].pos.clone();
            this.dream.mix(center);
        }

        const v = new Victor(5, 5);
        v.multiply(new Victor(
            this.pos.distanceX(this.dream) > 10 ? -this.speed : this.speed,
            this.pos.distanceY(this.dream) > 10 ? -this.speed : this.speed
        ));
        v.multiplyScalar(speed);

        let inAny = false;
        const pos = this.pos.clone().add(v);
        for (let i = 0; i < positions.length - 1; i++) {
            const p0 = positions[i].pos;
            const p1 = positions[i + 1].pos;

            if (util.inTriangle(pos, p0, p1, center)) {
                inAny = true;
                break;
            }
        }

        if (inAny) {
            this.pos.add(v);
        }
        return this;
    }

    follow(whom) {
        let scale = this.size * 2 + whom.size * 2;

        let mixes = [
            whom.pos.clone().add(new Victor(scale, scale)),
            whom.pos.clone().add(new Victor(-scale, scale)),
            whom.pos.clone().add(new Victor(scale, -scale)),
            whom.pos.clone().add(new Victor(-scale, -scale)),
        ];

        this.pos.mix(mixes[~~(Math.random() * mixes.length)], this.speed);
        return this;
    }

    draw(ctx) {
        const { hue, dead } = this;
        const { x, y } = this.pos;

        ctx.beginPath();
        ctx.arc(x, y, this.size, 0, Math.PI * 2);

        if (dead) {
            ctx.fillStyle = `hsla(${hue}, 100%, 25%, 0.2)`;
        } else {
            ctx.fillStyle = `hsl(${hue}, 100%, 25%)`;
        }
        ctx.fill();
    }
}

export default Creature;
