#!/bin/bash

echo "#### DEPLOY ####"

yarn build

zip -9 -r tmp.zip build

ssh mcor@62.210.69.116 "cd /home/mcor/craete/app && rm radar -rf"
scp tmp.zip mcor@62.210.69.116:~/craete/app/tmp.zip
ssh mcor@62.210.69.116 "cd /home/mcor/craete/app && unzip -o -j tmp.zip -d radar"

rm -rf tmp.zip
exit 0
