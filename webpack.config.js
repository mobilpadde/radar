const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const name = `[name]`;
const ext = `[ext]`;
const hash = `[hash:7]`;
const filename = `${name}.${ext}?v=${hash}`;
const buildPath = path.resolve(__dirname, 'public');

module.exports = {
    entry: {
        index: './src/js/main.js',
    },

    devtool: 'eval',

    output: {
        filename: `${name}.bundle.js?v=${hash}`,
        path: buildPath,
    },

    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'common',
            filename: `${name}.js?v=${hash}`,
            minChunks: Infinity,
        }),
        new HtmlWebpackPlugin({
            chunks: ['index'],
            template: './src/views/index.pug',
            filename: 'index.html',
        }),
        new CleanWebpackPlugin(buildPath),
        new webpack.optimize.UglifyJsPlugin(),
    ],

    module: {
        loaders: [{
            test: /\.pug$/,
            exclude: /(node_modules)/,
            loader: 'pug-loader'
        }, {
            test: /\.js$/,
            exclude: /(node_modules)/,
            loader: `babel-loader?name=${filename}`,
            options: {
                presets: ['es2015']
            },
        }, {
            test: /\.styl$/,
            exclude: /(node_modules)/,
            use: [`style-loader?name=${filename}`, 'css-loader', 'stylus-loader']
        }, {
            test: /\.(eot|svg|ttf|woff2?)$/,
            exclude: /(node_modules)/,
            loader: `file-loader?name=font/${filename}`
        }]
    }
};
